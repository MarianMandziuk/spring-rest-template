package com.and.company.controller;


import com.and.company.dto.ErrorDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpStatusCodeException;

import java.time.LocalDateTime;

@Slf4j
@RestControllerAdvice
public class ErrorHandlingController {

    @ExceptionHandler(HttpStatusCodeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDto handleHttpStatusCodeException(HttpStatusCodeException ex) throws JsonProcessingException {
        log.error("handleHttpStatusCodeException: message {}", ex.getMessage());
        return new ErrorDto(ex.getMessage(), "Api errors", LocalDateTime.now());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDto handleException(Exception ex) {
        log.error("handleException: message {}", ex.getMessage());
        return new ErrorDto(ex.getMessage(), "Internal errors", LocalDateTime.now());
    }

}
