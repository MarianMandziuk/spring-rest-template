package com.and.company.controller;

import com.and.company.dto.AccountDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@Slf4j
@RestController
@RequiredArgsConstructor
public class RestTemplateController {

    private static final String BASE = "http://localhost:8080/account/";

    private final RestTemplate restTemplate;

    @GetMapping("/get/{email}")
    public AccountDto getAccountApi(@PathVariable String email) {
        log.info("Get account");
        String uri = BASE + email;
        return restTemplate.getForObject(uri, AccountDto.class);
    }

    @PostMapping("/post")
    public AccountDto postAccountApi(@RequestBody AccountDto accountDto) {
        log.info("Create account {}", accountDto);
        return restTemplate.postForObject(BASE,  new HttpEntity<>(accountDto), AccountDto.class);
    }

    @PutMapping("/put/{email}")
    public void putAccountApi(@PathVariable String email, @RequestBody AccountDto accountDto) {
        log.info("Change email of account {}", accountDto);
        String uri = BASE + email;
        restTemplate.put(uri, accountDto);
    }

    @DeleteMapping("/delete/{email}")
    public void deleteAccountApi(@PathVariable String email) {
        log.info("Delete account by email: {}", email);
        String uri = BASE + email;
        restTemplate.delete(uri, email);
    }
}
