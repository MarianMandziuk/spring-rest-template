package com.and.company.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class ErrorDto {
    private String message;
    private String errorType;
    private LocalDateTime timeStamp;
}
