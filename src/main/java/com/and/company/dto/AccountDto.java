package com.and.company.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDto {
    private Long id;
    @NotNull
    @Email
    private String email;
    private String password;
    private String passwordRepeat;
    @Min(2)
    private String name;
    private Long roleId;
    private Long loyaltyId;
}

